@extends('forecast.main')

@section('content')
    <p class="lead">{{  $forecast->city_name }}</p>
    <h1 class="cover-heading">{{  $forecast->temperature }}°</h1>
    <p class="lead">{{  $forecast->weather_description }}</p>
    <p class="lead"><img src="http://openweathermap.org/img/w/{{ $forecast->weather_icon }}.png"></p>
    <p class="text-sm-center">Last update:<br/>{{  $forecast->updated_at->diffForHumans() }}</p>
@endsection
