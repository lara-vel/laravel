<?php

namespace App\Http\Controllers;

use App\Forecast;

class ForecastController extends Controller
{
    public function index()
    {
        $forecast = Forecast::where('city_id', '=', 756135)->first();

        if($forecast === null)
        {
            $forecast = $this->store();
        }
        else if ($forecast->updated_at<now()->subHour(1))
        {
            $forecast = $this->update();
        }



        return view('forecast.index', compact('forecast'));
    }

    public function store()
    {
        //warszawa
        $url = "http://api.openweathermap.org/data/2.5/forecast?id=756135&appid=a375eb2d53b161ace6e72efe454df535&units=metric";

        $json = json_decode(file_get_contents($url), true);

        $f = new Forecast;
        $f->city_id = $json['city']['id'];
        $f->city_name = $json['city']['name'];
        $f->temperature = $json['list'][0]['main']['temp'];
        $f->weather_id = $json['list'][0]['weather'][0]['id'];
        $f->weather_main = $json['list'][0]['weather'][0]['main'];
        $f->weather_description = $json['list'][0]['weather'][0]['description'];
        $f->weather_icon = $json['list'][0]['weather'][0]['icon'];
        $f->save();

        return $f;
    }

    public function update()
    {
        //warszawa
        $url = "http://api.openweathermap.org/data/2.5/forecast?id=756135&appid=a375eb2d53b161ace6e72efe454df535&units=metric";

        $json = json_decode(file_get_contents($url), true);

        $f = Forecast::updateOrCreate(
            [
                'city_id' => $json['city']['id']
            ],
            [
                'city_name' => $json['city']['name'],
                'temperature' => $json['list'][0]['main']['temp'],
                'weather_id' => $json['list'][0]['weather'][0]['id'],
                'weather_main' => $json['list'][0]['weather'][0]['main'],
                'weather_description' => $json['list'][0]['weather'][0]['description'],
                'weather_icon' => $json['list'][0]['weather'][0]['icon']
            ]
        );

        return $f;
    }
}
