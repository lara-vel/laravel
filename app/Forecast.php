<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forecast extends Model
{
    protected $fillable = ['temperature', 'weather_id', 'weather_main', 'weather_description', 'weather_icon'];
}
